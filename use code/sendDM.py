import requests
import json
import xlrd
import xlwt

class SendDM:
    token = ""
    default_url = "https://slack.com/api/"

    # set token when make senddm obj
    def __init__(self, token):
        self.token = token

    def getListOfUser(self):
        list = []
        r = requests.get(self.default_url + "users.list?token=" + self.token + "&pretty=1") 

        dict = json.loads(r.text)
        for h in dict['members']:
            list.append((h['id'], h['name']))

        return list


    def makeUserListTxtFile(self, path):
        f = open(path, 'w')
        list = self.getListOfUser()
        for i in list:
            f.write(i[1])
            f.write("\t\n")

    def makeUserListExcelFile(self, path):
        workbook = xlwt.Workbook(encoding='utf-8')
        workbook.default_style.font.height = 20 * \
                                             11

        worksheet = workbook.add_sheet('sheet0')
 
        font_style = xlwt.easyxf('font:height 280;')

        worksheet.write(0, 0, 'name')
        worksheet.write(0, 1, 'score')

        list = self.getListOfUser()
        for i in range(1, len(list) + 1):
            temp = list[i - 1][1]
            worksheet.write(i, 0, temp)

        workbook.save(path)




    def getChannelId(self, userID):
        r = requests.get(self.default_url + "im.open?token=" + self.token + "&user=" + userID + "&pretty=1")
        return json.loads(r.text)['channel']['id']

    def sendDMToAllUsingTxt(self, path):
        f = open(path, 'r')
        list = self.getListOfUser()
        while(1):
            line = f.readline()
            if not line: break
            line = line.split('\t')
            index = -1
            for i in list:
                if line[0] == i[1]:
                    index = list.index(i)
                    break

            if index > 0:
                message = line[1]
                id = list[index][0]

                channelid = self.getChannelId(id)
                r = requests.get(self.default_url + "chat.postMessage?token=" + self.token + "&channel=" + channelid + "&text=" + message + "&pretty=1")

    def sendDMToAllUsingExcel(self, path):
        workbook = xlrd.open_workbook(path)
        worksheet = workbook.sheet_by_index(0)
        nrows = worksheet.nrows
        
        row_val = []
        for row_num in range(nrows):
            row_val.append(worksheet.row_values(row_num))

        list = self.getListOfUser()

        for temp in row_val:
            index = -1
            for i in list:
                if temp[0] == i[1]:
                    index = list.index(i)
                    break

            if index > 0:
                message = temp[1]
                id = list[index][0]
                # 여기서 빈값 check
                # 파일에 있는 문자 앞뒤로 고정 스트링

                channelid = self.getChannelId(id)
                r = requests.get(self.default_url + "chat.postMessage?token=" + self.token + "&channel=" + channelid + "&text=" + str(message) + "&pretty=1")
