import requests
import json
import xlwt
import sys
import os

file = open("slack_token", "r")
token = file.read().split("\n")[0]

default_url = "https://slack.com/api/"


def getListOfUser():
    list = []
    r = requests.get(default_url + "users.list?token=" + token + "&pretty=1")

    dict = json.loads(r.text)
    for h in dict['members']:
        list.append((h['id'], h['name']))

    return list


def makeUserListTxtFile(path):
    f = open(path, 'w')
    list = getListOfUser()
    for i in list:
        f.write(i[1])
        f.write("\t\n")


def makeUserListExcelFile(path):
    workbook = xlwt.Workbook(encoding='utf-8')
    workbook.default_style.font.height = 20 * 11

    worksheet = workbook.add_sheet('sheet0')

    font_style = xlwt.easyxf('font:height 280;')

    worksheet.write(0, 0, 'name')
    worksheet.write(0, 1, 'score')

    list = getListOfUser()
    for i in range(1, len(list) + 1):
        temp = list[i - 1][1]
        worksheet.write(i, 0, temp)

    workbook.save(path)

# convert current path when path argv is "."
def convertPath(path):
    if path == ".":
        return os.getcwd() + "/list"
    else:
        return path

if __name__ == "__main__":
    if len(sys.argv) == 1 or sys.argv[1] == "--h":
        print("Have to take 3 argv.\n SD_create_file <file_path> <extension>")
        print("ex) python SD_create_file.py . txt")
        print("-> create txt file at current path")
        print()
        print("--h ")
        sys.exit(1)

    if sys.argv[2] == "txt":
        makeUserListTxtFile(convertPath(sys.argv[1]) + ".txt")
    elif sys.argv[2] == "xls":
        makeUserListExcelFile(convertPath(sys.argv[1]) + ".xls")
