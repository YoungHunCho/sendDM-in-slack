import sendDM
import os

# file_path = "/Users/cho/Desktop/ba.xls"
# file_path = "C:\\Users\\user\\Desktop\\q.txt"
file_path = os.getcwd()
file_path += "/list.xls"

file = open("slack_token", "r")
token = file.read().split("\n")

dm = sendDM.SendDM(token[0])
# dm.makeUserListTxtFile(file_path)
# dm.sendDMToAllUsingTxt(file_path)

# dm.makeUserListExcelFile(file_path)
dm.sendDMToAllUsingExcel(file_path)