import requests
import json
import xlrd
import SD_create_file as cf

file = open("slack_token", "r")
token = file.read().split("\n")[0]

default_url = "https://slack.com/api/"


def getChannelId(userID):
    r = requests.get(default_url + "im.open?token=" + token + "&user=" + userID + "&pretty=1")
    return json.loads(r.text)['channel']['id']

def sendDMToAllUsingTxt(path, front, back):
    f = open(path, 'r')
    list = cf.getListOfUser()
    while (1):
        line = f.readline()
        if not line: break
        line = line.split('\t')
        index = -1
        for i in list:
            if line[0] == i[1]:
                index = list.index(i)
                break

        if index > 0:
            message = line[1]
            id = list[index][0]
            if message == "":
                continue
            message = str(front) + str(message) + str(back)
            channelid = getChannelId(id)
            r = requests.get(
                default_url + "chat.postMessage?token=" + token + "&channel=" + channelid + "&text=" + message + "&pretty=1")


def sendDMToAllUsingExcel( path, front, back):
    workbook = xlrd.open_workbook(path)
    worksheet = workbook.sheet_by_index(0)
    nrows = worksheet.nrows

    row_val = []
    for row_num in range(nrows):
        row_val.append(worksheet.row_values(row_num))

    list = cf.getListOfUser()

    for temp in row_val:
        index = -1
        for i in list:
            if temp[0] == i[1]:
                index = list.index(i)
                break

        if index > 0:
            message = temp[1]
            id = list[index][0]
            if message == "":
                continue
            message = str(front) + str(message) + str(back)
            channelid = getChannelId(id)
            r = requests.get(
                default_url + "chat.postMessage?token=" + token + "&channel=" + channelid + "&text=" + str(
                    message) + "&pretty=1&username=cho")

import sys


if __name__ == "__main__":
    if len(sys.argv) == 1 or sys.argv[1] == "--h":
        print("Have to take 2 argv.\n SD_send_file <file_path>")
        print("ex) python SD_create_file.py list.xls")
        print()
        print("--h ")
        sys.exit(1)

    type = sys.argv[1].split(".")[1]

    front = input("write front text:")
    back = input("write back text:")
    if type == "txt":
        sendDMToAllUsingTxt(sys.argv[1], front, back)
    elif type == "xls":
        sendDMToAllUsingExcel(sys.argv[1], front, back)
